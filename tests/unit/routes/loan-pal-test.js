import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | loan-pal', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:loan-pal');
    assert.ok(route);
  });
});
