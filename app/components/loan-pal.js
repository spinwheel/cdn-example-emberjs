import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

class LoanPal extends Component {
    @tracked isLoanConnected = false;
    @action handleLoanConnected(token){
        this.isLoanConnected = true;
        const handler = Spinwheel.create({
            containerId: 'loan-pal-dim-container',
            env: 'qa',
            dropinConfig: {
                module: 'loan-pal',
                token
            },
        });
        handler.open();
    }
}

export default LoanPal;
