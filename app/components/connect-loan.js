import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

class ConnectLoanComponent extends Component {
    @tracked loanConnected = false;
    @tracked loanConnecting = false;
    @tracked showSelfContent = true;
    @tracked token = '';

    @action
    doConnect() {
        this.showSelfContent = false;
        this.loanConnecting = true;
        const handler = Spinwheel.create({
            containerId: 'connect-loan-dim-container',
            env: 'qa',
            onSuccess: response => {
                this.token = response.metadata.token;
                this.loanConnecting = false;
                this.loanConnected = true;
                if(this.args.onSuccess){
                    this.args.onSuccess(this.token);
                }else {
                    this.showSelfContent = true;
                }
            },
            dropinConfig: {
                header: 0,
                module: 'loan-servicers-login',
                token: '<PASS TOKEN HERE>',
            },
        });
        handler.open();
    }
}
export default ConnectLoanComponent;

